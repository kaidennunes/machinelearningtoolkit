﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace MLToolkitCSharp
{
	/// <summary>
	/// Class for parsing out the command line arguments
	/// </summary>
	public class ArgParser
	{
		// C# style properties with getters and setters
		public string ARFF { private set; get; }
		public string Learner { private set; get; }
		public string Evaluation { private set; get; }
		public string EvalParameter { private set; get; }
		public bool Verbose { private set; get; }
		public bool Normalize { private set; get; }

		// You can add more options for specific learning models if you wish
		public ArgParser(string[] argv)
		{
			try
			{
				for (int i = 0; i < argv.Length; i++)
				{

					if (argv[i].Equals("-V"))
					{
						Verbose = true;
					}
					else if (argv[i].Equals("-N"))
					{
						Normalize = true;
					}
					else if (argv[i].Equals("-A"))
					{
						ARFF = argv[++i];
					}
					else if (argv[i].Equals("-L"))
					{
						Learner = argv[++i];
					}
					else if (argv[i].Equals("-E"))
					{
						Evaluation = argv[++i];
						if (argv[i].Equals("static"))
						{
							//expecting a test set name
							EvalParameter = argv[++i];
						}
						else if (argv[i].Equals("random"))
						{
							//expecting a double representing the percentage for testing
							//Note stratification is NOT done
							EvalParameter = argv[++i];
						}
						else if (argv[i].Equals("cross"))
						{
							//expecting the number of folds
							EvalParameter = argv[++i];
						}
						else if (!argv[i].Equals("training"))
						{
							Console.WriteLine("Invalid Evaluation Method: " + argv[i]);
							Environment.Exit(0);
						}
					}
					else
					{
						Console.WriteLine("Invalid parameter: " + argv[i]);
						Environment.Exit(0);
					}
				}

			}
			catch (Exception)
			{
				Console.WriteLine("Usage:");
				Console.WriteLine("MLSystemManager -L [learningAlgorithm] -A [ARFF_File] -E [evaluationMethod] {[extraParamters]} [OPTIONS]\n");
				Console.WriteLine("OPTIONS:");
				Console.WriteLine("-V Print the confusion matrix and learner accuracy on individual class values\n");

				Console.WriteLine("Possible evaluation methods are:");
				Console.WriteLine("MLSystemManager -L [learningAlgorithm] -A [ARFF_File] -E training");
				Console.WriteLine("MLSystemManager -L [learningAlgorithm] -A [ARFF_File] -E static [testARFF_File]");
				Console.WriteLine("MLSystemManager -L [learningAlgorithm] -A [ARFF_File] -E random [%_ForTesting]");
				Console.WriteLine("MLSystemManager -L [learningAlgorithm] -A [ARFF_File] -E cross [numOfFolds]\n");
				Environment.Exit(0);
			}

			if (ARFF == null || Learner == null || Evaluation == null)
			{
				Console.WriteLine("Usage:");
				Console.WriteLine("MLSystemManager -L [learningAlgorithm] -A [ARFF_File] -E [evaluationMethod] {[extraParamters]} [OPTIONS]\n");
				Console.WriteLine("OPTIONS:");
				Console.WriteLine("-V Print the confusion matrix and learner accuracy on individual class values");
				Console.WriteLine("-N Use normalized data");
				Console.WriteLine();
				Console.WriteLine("Possible evaluation methods are:");
				Console.WriteLine("MLSystemManager -L [learningAlgorithm] -A [ARFF_File] -E training");
				Console.WriteLine("MLSystemManager -L [learningAlgorithm] -A [ARFF_File] -E static [testARFF_File]");
				Console.WriteLine("MLSystemManager -L [learningAlgorithm] -A [ARFF_File] -E random [%_ForTesting]");
				Console.WriteLine("MLSystemManager -L [learningAlgorithm] -A [ARFF_File] -E cross [numOfFolds]\n");
				Environment.Exit(0);
			}
		}
	}
}
