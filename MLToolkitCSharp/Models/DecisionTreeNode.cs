﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace MLToolkitCSharp.Models
{
	public class DecisionTreeNode
	{
		public DecisionTreeNode()
		{
			ChildrenNodes = new List<DecisionTreeNode>();
			Output = -1;
			FeatureIndex = -1;
		}
		public List<DecisionTreeNode> ChildrenNodes { get; set; }
		public int FeatureIndex { get; set; }
		public double Output { get; set; }

		public int GetNodeCount()
		{
			int nodeCount = 1;
			foreach (var childNode in ChildrenNodes)
			{
				nodeCount += childNode.GetNodeCount();
			}
			return nodeCount;
		}

		public int GetDepth()
		{
			List<int> nodeDepths = new List<int>();
			foreach (var childNode in ChildrenNodes)
			{
				nodeDepths.Add(childNode.GetDepth());
			}

			return nodeDepths.Count != 0 ? nodeDepths.Max() + 1 : 1;
		}
	}
}
