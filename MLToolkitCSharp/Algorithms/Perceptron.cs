﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;

namespace MLToolkitCSharp.Algorithms
{
	public class Perceptron : SupervisedLearner
	{
		private static readonly string CSV_PATH = Utils.ROOT_PATH + "CSV\\Perceptron.csv";

		public List<double[]> Weights;

		private readonly Random RandomGenerator;

		private static readonly double LEARNING_RATE = 0.1;

		private static readonly int MAX_EPOCHS_WITHOUT_IMPROVEMENT = 10;

		public Perceptron() : this(new Random()) { }

		public Perceptron(Random random)
		{
			RandomGenerator = random;
			Weights = new List<double[]>();
		}

		public override void Train(Matrix features, Matrix labels)
		{
			var numberOfClassifications = labels.StringToEnum.FirstOrDefault().Count;

			if (numberOfClassifications == 2)
			{
				Weights.Add(Train(features, labels, 1));
			}
			else
			{
				for (int i = 0; i < numberOfClassifications; i++)
				{
					Weights.Add(Train(features, labels, i));
				}
			}
		}

		private double[] Train(Matrix features, Matrix labels, int outputClass)
		{
			// Keep a string with CSV data so we can plot accuracy vs epochs
			StringBuilder csvData = new StringBuilder();

			// Initialize the weights with the number of columns in our features, plus 1 for the bias weight
			var weights = new double[features.Cols() + 1];

			// Keep track of the instances solved by previous iterations and the number of epochs that don't improve anything
			int mostInstancesSolved = 0;
			int epochsWithoutImprovement = 0;

			// Keep track of total number of epochs run
			int totalEpochs = 0;

			// Start the training
			while (true)
			{
				// Keep track of the instances solved in this epoch
				int instancesSolved = 0;

				// Shuffle the features before each epoch
				features.Shuffle(RandomGenerator, labels);

				// Go through all of the patterns (instances)
				for (int i = 0; i < features.Rows(); i++)
				{
					// Find the current pattern index
					int currentPatternIndex = i;

					// Initialize the net output variable for that pattern
					double netOutput = 0;

					// Go through the inputs for that instance
					int j;
					for (j = 0; j < weights.Length - 1; j++)
					{
						var input = features.Get(currentPatternIndex, j);
						netOutput += input * weights[j];
					}

					// Then go through the bias weight (which will always be the last weight, with input 1)
					netOutput += weights[j];

					// Convert the net output to either firing (1 or true) or non firing (0 false)
					var output = netOutput >= 1 ? 1 : 0;

					// Get the target value. 0 is false (non firing) and 1 is true (firing)
					var target = labels.Get(currentPatternIndex, 0);

					target = target == outputClass ? 1 : 0;

					// If we didn't make our target, then adjust the weights
					if (target != output)
					{
						// Update each weight for the inputs
						int k;
						for (k = 0; k < weights.Length - 1; k++)
						{
							// Get the input
							var input = features.Get(currentPatternIndex, k);

							// Change of w = c(t – z)x
							weights[k] += LEARNING_RATE * (target - output) * input;
						}

						// Then update the bias weight
						weights[k] += LEARNING_RATE * (target - output);
					}
					else
					{
						instancesSolved++;
					}
				}

				// Keep track of the accuracy vs the epochs run
				double trainingEpochMisclassification = 1 - (instancesSolved / (double)features.Rows());
				csvData.Append(totalEpochs);
				csvData.Append(", ");
				csvData.Append(trainingEpochMisclassification);
				csvData.Append(", ");
				csvData.Append("\n");

				// Increment the total number of epochs run
				totalEpochs++;

				bool improved = false;
				if (instancesSolved > mostInstancesSolved)
				{
					improved = true;
					mostInstancesSolved = instancesSolved;
				}

				// Keep track of whether anything improved in that epoch
				epochsWithoutImprovement = improved ? 0 : epochsWithoutImprovement + 1;

				// Determine whether we should quit
				if (MAX_EPOCHS_WITHOUT_IMPROVEMENT <= epochsWithoutImprovement)
				{
					break;
				}
			}

			// Print to the CSV file
			File.WriteAllText(CSV_PATH, csvData.ToString());

			// Print out debugging information
			Console.WriteLine("Total epochs run: " + totalEpochs);
			for (int i = 0; i < weights.Length; i++)
			{
				Console.WriteLine("Weight " + i + ": " + weights[i]);
			}

			return weights;
		}

		public override void Predict(double[] features, double[] labels)
		{
			var numberOfClassifications = Weights.Count + 1;
			if (numberOfClassifications == 2)
			{
				BasicPredict(features, labels);
				return;
			}

			double highestNetOutput = 0;
			int classification = 0;
			for (int i = 0; i < numberOfClassifications - 1; i++)
			{
				var weights = Weights[i];
				double netOutput = 0;
				int j;
				for (j = 0; j < weights.Length - 1; j++)
				{
					var input = features[j];
					netOutput += input * weights[j];
				}
				netOutput += weights[j];

				if (highestNetOutput < netOutput)
				{
					highestNetOutput = netOutput;
					classification = i;
				}

			}

			labels[0] = classification;

		}

		private void BasicPredict(double[] features, double[] labels)
		{
			var weights = Weights[0];
			double netOutput = 0;
			int i;
			for (i = 0; i < weights.Length - 1; i++)
			{
				var input = features[i];
				netOutput += input * weights[i];
			}
			netOutput += weights[i];

			// Convert the net output to either firing (1 or true) or non firing (0 false)
			var output = netOutput >= 1 ? 1 : 0;

			labels[0] = output;
		}
	}
}
