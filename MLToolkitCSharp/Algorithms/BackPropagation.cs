﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;

namespace MLToolkitCSharp.Algorithms
{
	public class BackPropagation : SupervisedLearner
	{
		private enum NodeType
		{
			HIDDEN,
			OUTPUT
		}

		private static readonly string CSV_PATH = Utils.ROOT_PATH + "CSV\\BackPropagation.csv";

		private readonly Random RandomGenerator;

		private static readonly double TRAINING_PERCENTAGE = 0.5;

		private static readonly double LEARNING_RATE = 0.5;

		private static readonly double MOMENTUM = 0.5;

		private static readonly int NUMBER_OF_HIDDEN_NODES_PER_LAYER = 64;

		private static readonly int NUMBER_OF_HIDDEN_LAYERS = 1;

		private static readonly int MAX_EPOCHS_WITHOUT_IMPROVEMENT = 50;

		private int NumberOfOutputs { get; set; }

		private int NumberOfInputs { get; set; }

		private List<double[][]> Weights { get; set; }

		private List<double[][]> PreviousWeightChange { get; set; }

		public BackPropagation() : this(new Random()) { }

		public BackPropagation(Random random)
		{
			RandomGenerator = random;
			Weights = new List<double[][]>();
			PreviousWeightChange = new List<double[][]>();
		}

		public override void Train(Matrix features, Matrix labels)
		{
			// Number of output classifications
			NumberOfOutputs = labels.StringToEnum.FirstOrDefault().Count;

			// Number of inputs
			NumberOfInputs = features.Cols();

			// Create all the initial weights
			InitializeWeights();

			// Keep track of the total number of epochs run
			int totalEpochsRun = 0;

			// Keep track of the best number of instances solved, the weights, the number of epochs, and the validation accuracy
			int mostInstancesSolved = 0;
			int bestEpochNumber = 0;
			List<double[][]> bestWeights = Weights;
			double bestValidationMSE = 0;

			// Keep track of epochs run without improvement
			int epochsWithoutImprovement = 0;

			// Get the number of instances in the set
			int numberOfInstances = features.Rows();

			// Find the split in instances for the training and validation set
			int instanceSplit = (int)(TRAINING_PERCENTAGE * numberOfInstances);

			// Split set into training and validation set
			Matrix trainingSetFeatures = new Matrix(features, 0, 0, instanceSplit, NumberOfInputs);
			Matrix trainingSetLabels = new Matrix(labels, 0, 0, instanceSplit, 1);
			Matrix validationSetFeatures = new Matrix(features, instanceSplit, 0, numberOfInstances - instanceSplit, NumberOfInputs);
			Matrix validationSetLabels = new Matrix(labels, instanceSplit, 0, numberOfInstances - instanceSplit, 1);

			// Begin training
			while (epochsWithoutImprovement < MAX_EPOCHS_WITHOUT_IMPROVEMENT)
			{
				// Keep track of the instances solved in this epoch
				int instancesSolved = 0;

				// Shuffle the features before each epoch
				trainingSetFeatures.Shuffle(RandomGenerator, trainingSetLabels);

				for (int instanceIndex = 0; instanceIndex < trainingSetFeatures.Rows(); instanceIndex++)
				{
					// Format the target output
					double[] targets = new double[NumberOfOutputs];
					var outputClass = (int)trainingSetLabels.Get(instanceIndex, 0);
					targets[outputClass] = 1.0;

					// Calculate the outputs for the network
					double[][] nodeOutputs = CalculateOutputs(trainingSetFeatures.Row(instanceIndex));

					// Update the weights for the network
					bool solvedInstance = UpdateWeights(trainingSetFeatures.Row(instanceIndex), targets, nodeOutputs);

					// Update the number of solved instances for that epoch
					if (solvedInstance)
					{
						instancesSolved++;
					}
				}
				// Update the number of epochs run
				totalEpochsRun++;

				// Find the MSE using the validation set
				double validationMSE = 0;
				for (int i = 0; i < validationSetFeatures.Rows(); i++)
				{
					double[] validationInstance = validationSetFeatures.Row(i);
					// Format the target output
					double[] validationTargets = new double[NumberOfOutputs];
					var validationOutputClass = (int)validationSetLabels.Get(i, 0);
					validationTargets[validationOutputClass] = 1.0;

					double[][] validationNodeOutputs = CalculateOutputs(validationInstance);
					for (int j = 0; j < validationNodeOutputs[validationNodeOutputs.Length - 1].Length; j++)
					{
						double error = (validationTargets[j] - validationNodeOutputs[validationNodeOutputs.Length - 1][j]);
						validationMSE += error * error;
					}
				}
				validationMSE /= NumberOfOutputs;

				// Update the best found solution
				if (bestValidationMSE > validationMSE || totalEpochsRun == 1)
				{
					bestValidationMSE = validationMSE;
					mostInstancesSolved = instancesSolved;
					bestEpochNumber = totalEpochsRun;
					bestWeights = CloneWeights(Weights);
					epochsWithoutImprovement = 0;
				}
				else
				{
					epochsWithoutImprovement++;
				}
			}

			Console.WriteLine("Best Solution");
			Console.WriteLine("Training set solved: " + mostInstancesSolved);
			Console.WriteLine("Total training instances: " + trainingSetFeatures.Rows());
			Console.WriteLine("Epochs run: " + bestEpochNumber);
			Console.WriteLine("Total Epochs run: " + totalEpochsRun);

			// Use the best weights found
			Weights = bestWeights;
		}

		public override void Predict(double[] features, double[] labels)
		{
			double[][] nodeOutputs = CalculateOutputs(features);
			labels[0] = CalculateOutputIndex(nodeOutputs);
		}

		private int CalculateOutputIndex(double[][] nodeOutputs)
		{
			int nodeOutputIndex = 0;
			double highestNodeOutput = nodeOutputs[Weights.Count - 1][0];
			for (int i = 1; i < nodeOutputs[Weights.Count - 1].Length; i++)
			{
				var nodeOutput = nodeOutputs[Weights.Count - 1][i];
				if (nodeOutput > highestNodeOutput)
				{
					highestNodeOutput = nodeOutput;
					nodeOutputIndex = i;
				}
			}
			return nodeOutputIndex;
		}

		private double[][] CalculateOutputs(double[] instance)
		{
			// Store the node outputs for the entire network
			double[][] nodeOutputs = new double[Weights.Count][];

			// Go through each layer
			for (int i = 0; i < Weights.Count; i++)
			{
				// Store this layer's node outputs
				nodeOutputs[i] = new double[Weights[i].Length];

				// Go through each node for that layer (not including the bias node)
				for (int j = 0; j < Weights[i].Length; j++)
				{
					// Store the net value for this node
					double netValue = 0;

					// For each weight leading to that node, calculate the value of that node/feature
					for (int k = 0; k < Weights[i][j].Length; k++)
					{
						// Get the weight for that node connection
						var weightValue = Weights[i][j][k];

						// Find the input value for that node connection, using 1 for the bias node
						double inputValue = 0;

						// If this is the first layer, then the input is the feature. Otherwise, the input is the output from the previous nodes, or the bias node
						if (i == 0)
						{
							inputValue = Weights[i][j].Length - 1 == k ? 1 : instance[k];
						}
						else if (k == Weights[i][j].Length - 1)
						{
							inputValue = 1;
						}
						else
						{
							inputValue += nodeOutputs[i - 1][k];

						}

						// Update the net value for this node
						netValue += weightValue * inputValue;
					}

					// Find the output for this node
					nodeOutputs[i][j] = CalculateNodeOutput(netValue);
				}
			}

			return nodeOutputs;
		}

		private bool UpdateWeights(double[] inputs, double[] targets, double[][] nodeOutputs)
		{
			double[] nodeSigmas = new double[0];

			List<double[][]> updatedWeights = CloneWeights(Weights);

			// Go through the layers
			for (int i = Weights.Count - 1; i >= 0; i--)
			{
				// Keep track of the previous layer's sigma values
				double[] currentNodeSigmas = new double[Weights[i].Length];

				// Go through each node for that layer (not including the bias node)
				for (int j = 0; j < Weights[i].Length; j++)
				{
					// Get the output for this node
					var nodeOutput = nodeOutputs[i][j];

					// Find what type of node is being processed
					var nodeType = i == Weights.Count - 1 ? NodeType.OUTPUT : NodeType.HIDDEN;

					// Get the node's net derivative
					double nodeNetDerivative = nodeOutput * (1 - nodeOutput);

					// Keep track of this node's sigma
					double nodeSigma = nodeNetDerivative;

					// Find the sigma for that node
					if (NodeType.OUTPUT == nodeType)
					{
						nodeSigma *= (targets[j] - nodeOutput);
					}
					else
					{
						double sumOfUpperLayer = 0;
						for (int k = 0; k < Weights[i + 1].Length; k++)
						{
							sumOfUpperLayer += Weights[i + 1][k][j] * nodeSigmas[k];
						}
						nodeSigma *= sumOfUpperLayer;
					}

					// Save the sigma for use later
					currentNodeSigmas[j] = nodeSigma;

					// For each weight leading to that node, update the weight
					for (int k = 0; k < Weights[i][j].Length; k++)
					{
						// For the bias weight, the output is always 1
						double previousNodeOutput = 1;

						// Get the previous node's output
						if (k < Weights[i][j].Length - 1)
						{
							// Look at the last node if this isn't the first layer, otherwise, look at the feature inputs
							if (i != 0)
							{
								previousNodeOutput = nodeOutputs[i - 1][k];
							}
							else
							{
								previousNodeOutput = inputs[k];
							}
						}

						var weightChange = CalculateWeightChange(previousNodeOutput, nodeSigma, PreviousWeightChange[i][j][k]);
						PreviousWeightChange[i][j][k] = weightChange;
						updatedWeights[i][j][k] += weightChange;
					}
				}

				// Save the node sigmas for use in the next layer
				nodeSigmas = currentNodeSigmas;
			}

			// Update the weights
			Weights = updatedWeights;

			// Find out which of the output nodes had the highest value
			int nodeOutputIndex = CalculateOutputIndex(nodeOutputs);

			if (targets[nodeOutputIndex] >= 0.99)
			{
				return true;
			}
			else
			{
				return false;
			}
		}

		private List<double[][]> CloneWeights(List<double[][]> list)
		{
			List<double[][]> clonedList = new List<double[][]>();
			foreach (var layer in list)
			{
				double[][] clonedLayer = new double[layer.Length][];
				for (int i = 0; i < layer.Length; i++)
				{
					clonedLayer[i] = (double[])layer[i].Clone();

				}
				clonedList.Add(clonedLayer);
			}
			return clonedList;
		}

		private double CalculateWeightChange(double startNodeOutput, double endNodeSigma, double previousWeightChange)
		{
			return (LEARNING_RATE * startNodeOutput * endNodeSigma) + (previousWeightChange * MOMENTUM);
		}

		private double CalculateNodeOutput(double netValue)
		{
			return 1 / (1 + Math.Pow(Math.E, (netValue * -1)));
		}

		private void InitializeWeights()
		{
			// Create first hidden layer weights
			Weights.Add(CreateLayerWeights(NumberOfInputs, NUMBER_OF_HIDDEN_NODES_PER_LAYER));
			PreviousWeightChange.Add(CreateLayerWeights(NumberOfInputs, NUMBER_OF_HIDDEN_NODES_PER_LAYER, true));

			// Add weights for the middle hidden layers
			for (int i = 0; i < NUMBER_OF_HIDDEN_LAYERS - 1; i++)
			{
				Weights.Add(CreateLayerWeights(NUMBER_OF_HIDDEN_NODES_PER_LAYER, NUMBER_OF_HIDDEN_NODES_PER_LAYER));
				PreviousWeightChange.Add(CreateLayerWeights(NUMBER_OF_HIDDEN_NODES_PER_LAYER, NUMBER_OF_HIDDEN_NODES_PER_LAYER, true));
			}

			// Add weights for the last hidden layer
			Weights.Add(CreateLayerWeights(NUMBER_OF_HIDDEN_NODES_PER_LAYER, NumberOfOutputs));
			PreviousWeightChange.Add(CreateLayerWeights(NUMBER_OF_HIDDEN_NODES_PER_LAYER, NumberOfOutputs, true));
		}

		/// <summary>
		/// Creates random small weights for a layer, plus a bias weight
		/// </summary>
		/// <param name="numberOfPreviousLayerNodes">Number of nodes in the previous layer, not counting the bias.</param>
		/// <param name="numberOfLayerNodes">Number of nodes in the layer, not counting the bias.</param>
		/// <returns></returns>
		private double[][] CreateLayerWeights(int numberOfPreviousLayerNodes, int numberOfLayerNodes, bool setToZero = false)
		{
			double[][] newWeights = new double[numberOfLayerNodes][];
			for (int i = 0; i < newWeights.Length; i++)
			{
				newWeights[i] = new double[numberOfPreviousLayerNodes + 1];
				for (int j = 0; j < newWeights[i].Length; j++)
				{
					newWeights[i][j] = setToZero ? 0 : (0.0009) * (RandomGenerator.NextDouble() - 0.5) + 0.0001;
				}
			}
			return newWeights;
		}
	}
}
