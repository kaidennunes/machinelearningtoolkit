﻿// ----------------------------------------------------------------
// The contents of this file are distributed under the CC0 license.
// See http://creativecommons.org/publicdomain/zero/1.0/
// ----------------------------------------------------------------

using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace MLToolkitCSharp
{
    public class BaselineLearner : SupervisedLearner
    {
        public double[] MatrixLabels;

        public override void Train(Matrix features, Matrix labels)
        {
            MatrixLabels = new double[labels.Cols()];
            for (int i = 0; i < labels.Cols(); i++)
            {
                if (labels.ValueCount(i) == 0)
                {
                    MatrixLabels[i] = labels.ColumnMean(i); // continuous
                }
                else
                {
                    MatrixLabels[i] = labels.MostCommonValue(i); // nominal
                }
            }
        }

        public override void Predict(double[] features, double[] labels)
        {
            for (int i = 0; i < MatrixLabels.Length; i++)
            {
                labels[i] = MatrixLabels[i];
            }
        }
    }
}
