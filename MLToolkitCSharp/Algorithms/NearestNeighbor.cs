﻿using MLToolkitCSharp.Models;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;

namespace MLToolkitCSharp.Algorithms
{
	public class NearestNeighbor : SupervisedLearner
	{
		private static readonly string CSV_PATH = Utils.ROOT_PATH + "CSV\\NearestNeighbor.csv";

		private readonly Random RandomGenerator;

		private int NumberOfOutputs { get; set; }

		private int NumberOfInputs { get; set; }

		private int NumberOfInstances { get; set; }

		private Matrix Features { get; set; }

		private Matrix Labels { get; set; }

		private static readonly int NUMBER_OF_NEIGHBORS = 13;

		private static readonly bool USE_DISTANCE_WEIGHTING = true;

		private static readonly bool USE_REDUCTION = true;

		public NearestNeighbor() : this(new Random()) { }

		public NearestNeighbor(Random random)
		{
			RandomGenerator = random;
		}

		public override void Train(Matrix features, Matrix labels)
		{
			Console.WriteLine("Number of neighbors: " + NUMBER_OF_NEIGHBORS);

			// Number of output classifications
			NumberOfOutputs = labels.StringToEnum.FirstOrDefault().Count;

			// Number of inputs
			NumberOfInputs = features.Cols();

			// Number of instances
			NumberOfInstances = features.Rows();

			// Keep track of the training set
			Features = new Matrix(features, 0, 0, NumberOfInstances, NumberOfInputs);
			Labels = new Matrix(labels, 0, 0, NumberOfInstances, 1);

			// If we shouldn't use reduction, then just stop now
			if (!USE_REDUCTION)
			{
				return;
			}

			// Otherwise, use reduction to cut down the training set
			Console.WriteLine("Number of training set instances: " + NumberOfInstances);

			// For each instance try getting rid of it and seeing if it would be classified correctly anyway
			for (int i = 0; i < NumberOfInstances; i++)
			{
				// Make a copy of the features and labels to be able to go back to them if necessary
				Matrix trainingSetFeatures = new Matrix(Features, 0, 0, NumberOfInstances, NumberOfInputs);
				Matrix trainingSetLabels = new Matrix(Labels, 0, 0, NumberOfInstances, 1);

				// Remove this instance from the current features and labels
				Features.RemoveInstance(Labels, i);
				NumberOfInstances = Features.Rows();

				// Get a copy of this instance's features
				double[] instanceFeatures = new double[NumberOfInputs];
				for (int j = 0; j < NumberOfInputs; j++)
				{
					instanceFeatures[j] = features.Get(i, j);
				}

				// Get the label of this instance
				var instanceLabel = labels.Get(i, 0);

				// See what the prediction would be without this instance
				double[] instanceLabels = new double[1];
				Predict(instanceFeatures, instanceLabels);

				// If the training instance wasn't classified correctly, then put the instance back in
				if (instanceLabels[0] != instanceLabel)
				{
					Features = trainingSetFeatures;
					Labels = trainingSetLabels;
				}
			}

			Console.WriteLine("Number of training set instances after reduction: " + NumberOfInstances);
		}

		public override void Predict(double[] features, double[] labels)
		{
			// Keep track of the distances
			List<InstanceDistance> distances = new List<InstanceDistance>();

			// Go through each instance to find the distances
			for (int i = 0; i < NumberOfInstances; i++)
			{
				// Get the total distance from that feature
				InstanceDistance distanceInstance = new InstanceDistance
				{
					InstanceIndex = i
				};

				// Go through each feature for that instance
				for (int j = 0; j < NumberOfInputs; j++)
				{
					var feature = Features.Get(i, j);
					distanceInstance.Distance += Math.Abs(feature - features[j]);
				}

				// Add that distance to the list
				distances.Add(distanceInstance);
			}

			// Determine how many neighbors should be used
			var numberOfNeighbors = USE_DISTANCE_WEIGHTING && NUMBER_OF_NEIGHBORS == -1 ? NumberOfInstances : NUMBER_OF_NEIGHBORS;

			// Sort list by smallest to largest
			var sortedKDistances = distances.OrderBy(listedDistanceInstance => listedDistanceInstance.Distance).Take(numberOfNeighbors).ToList();

			labels[0] = USE_DISTANCE_WEIGHTING ? GetLabelWithWeights(sortedKDistances) : GetLabelWithoutWeights(sortedKDistances);
		}

		private double GetLabelWithoutWeights(List<InstanceDistance> sortedKDistances)
		{
			return NumberOfOutputs == 0 ? GetRegressionWithoutWeights(sortedKDistances) : GetClassificationWithoutWeights(sortedKDistances);
		}

		private double GetRegressionWithoutWeights(List<InstanceDistance> sortedKDistances)
		{
			double sumOfLabels = 0;
			// Go through each of these instances and add them up
			for (int i = 0; i < sortedKDistances.Count(); i++)
			{
				// Get output label
				var outputLabel = Labels.Get(sortedKDistances[i].InstanceIndex, 0);
				sumOfLabels += outputLabel;
			}
			return sumOfLabels / sortedKDistances.Count();
		}

		private double GetClassificationWithoutWeights(List<InstanceDistance> sortedKDistances)
		{
			int[] outputLabelVotes = new int[NumberOfOutputs];
			// Go through each of these instances and find out which one has more votes
			for (int i = 0; i < sortedKDistances.Count(); i++)
			{
				// Get output label
				int outputLabel = (int)Labels.Get(sortedKDistances[i].InstanceIndex, 0);
				outputLabelVotes[outputLabel]++;
			}

			int maxValue = outputLabelVotes.Max();
			int maxIndex = outputLabelVotes.ToList().IndexOf(maxValue);
			return maxIndex;
		}

		private double GetLabelWithWeights(List<InstanceDistance> sortedKDistances)
		{
			return NumberOfOutputs == 0 ? GetRegressionWithWeights(sortedKDistances) : GetClassificationWithWeights(sortedKDistances);
		}

		private double GetClassificationWithWeights(List<InstanceDistance> sortedKDistances)
		{
			// Keep track of the output labels and their votes
			Dictionary<double, double> outputLabelVotes = new Dictionary<double, double>();

			// Go through each of these instances and find out which one has more votes
			for (int i = 0; i < sortedKDistances.Count(); i++)
			{
				// Get output label
				double outputLabel = Labels.Get(sortedKDistances[i].InstanceIndex, 0);

				// If the output label doesn't have any votes, add the label to the dictionary
				if (!outputLabelVotes.ContainsKey(outputLabel))
				{
					outputLabelVotes.Add(outputLabel, 0);
				}

				// Update the distance with this label (using weighting)
				outputLabelVotes[outputLabel] += 1 / (Math.Pow(sortedKDistances[i].Distance, 2));
			}

			// Return the output label that has the most votes
			return outputLabelVotes.Aggregate((x, y) => x.Value > y.Value ? x : y).Key;
		}

		private double GetRegressionWithWeights(List<InstanceDistance> sortedKDistances)
		{
			double sumOfWeightedLabels = 0;
			double sumOfWeights = 0;
			// Go through each of these instances and add them up
			for (int i = 0; i < sortedKDistances.Count(); i++)
			{
				// Get output label
				var outputLabel = Labels.Get(sortedKDistances[i].InstanceIndex, 0);
				// Calculate weight
				var weight = 1 / (Math.Pow(sortedKDistances[i].Distance, 2));
				sumOfWeightedLabels += outputLabel * weight;
				sumOfWeights += weight;
			}
			return sumOfWeightedLabels / sumOfWeights;
		}

		private class InstanceDistance
		{
			public double Distance { get; set; }
			public int InstanceIndex { get; set; }
		}
	}
}
