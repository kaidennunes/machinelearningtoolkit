﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;

namespace MLToolkitCSharp.Algorithms
{
	public class Cluster : SupervisedLearner
	{
		private static readonly string CSV_PATH = Utils.ROOT_PATH + "CSV\\Cluster.csv";

		private readonly Random RandomGenerator;

		private readonly ClusterDistanceType CLUSTER_DISTANCE_TYPE = ClusterDistanceType.Single;

		private readonly ClusterAlgorithm CLUSTER_ALGORITHM = ClusterAlgorithm.HAC;

		private readonly bool USE_RANDOM_CENTROIDS = false;

		private readonly bool USE_ALTERNATIVE_DISTANCE_MEASURE = true;

		private readonly int NUMBER_OF_CLUSTERS = 7;

		private int NumberOfOutputs { get; set; }

		private int NumberOfInputs { get; set; }

		private List<Clustering> Clusters { get; set; }

		public Cluster() : this(new Random()) { }

		public Cluster(Random random)
		{
			RandomGenerator = random;
			Clusters = new List<Clustering>();
		}

		public override void Train(Matrix features, Matrix labels)
		{
			// Number of output classifications
			NumberOfOutputs = labels.StringToEnum.FirstOrDefault().Count;

			// Number of inputs
			NumberOfInputs = features.Cols();

			if (CLUSTER_ALGORITHM == ClusterAlgorithm.HAC)
			{

				var distanceMatrix = CalculateDistanceMatrix(features);
				var clusters = InitializeHACClustering(features);
				Clusters = CreateHACClusters(clusters, distanceMatrix, NUMBER_OF_CLUSTERS);
				AddClusteringCentroids(features, Clusters);
			}
			else if (CLUSTER_ALGORITHM == ClusterAlgorithm.KMeans)
			{
				var centroids = InitializeKMeansCentroids(features, NUMBER_OF_CLUSTERS);
				var clusters = CalculateKMeansClustering(features, centroids);
				Clusters = CreateKMeansClusters(features, clusters);
			}
			else
			{
				throw new Exception("Unrecognized algorithm type");
			}

			PrintClustereringInformation(features, Clusters);
		}

		public override void Predict(double[] features, double[] labels)
		{
			labels[0] = 0;
		}

		private double CalculateClusterSSE(Matrix features, Clustering clustering)
		{
			double sse = 0;
			for (int i = 0; i < clustering.Nodes.Count; i++)
			{
				var nodeFeatures = features.Row(clustering.Nodes[i]);
				// We want the sum of the squared Euclidean distance, this function gives us just the Euclidean distance
				sse += Math.Pow(CalculateNodeDistances(features, clustering.Centroid.Features, nodeFeatures), 2);
			}
			return sse;
		}

		private double CalculateTotalClusteringSSE(Matrix features, List<Clustering> clusterings)
		{
			double sse = 0;
			foreach (var clustering in clusterings)
			{
				sse += CalculateClusterSSE(features, clustering);
			}
			return sse;
		}

		private void PrintClustereringInformation(Matrix features, List<Clustering> clusterings)
		{
			List<double> clusteringErrors = new List<double>();

			Console.WriteLine("Number of Clusters: {0}", clusterings.Count);
			for (int i = 0; i < clusterings.Count; i++)
			{
				StringBuilder centroidInformation = new StringBuilder();
				for (int j = 0; j < NumberOfInputs; j++)
				{
					var centroidFeature = clusterings[i].Centroid.Features[j];
					if (centroidFeature == double.MaxValue)
					{
						centroidInformation.Append("?");
					}
					else if (features.EnumToString[j].Count != 0)
					{
						centroidInformation.Append(features.EnumToString[j][(int)centroidFeature]);
					}
					else
					{
						centroidInformation.Append(centroidFeature);

					}
					if (j + 1 != NumberOfInputs)
					{
						centroidInformation.Append(", ");
					}
				}
				// SSE of clustering
				var clusteringError = CalculateClusterSSE(features, clusterings[i]);
				clusteringErrors.Add(clusteringError);
				Console.WriteLine("Instances in Cluster: {0}", clusterings[i].Nodes.Count);
				Console.WriteLine("Centroid {0}: {1}", i + 1, centroidInformation.ToString());
				Console.WriteLine("Cluster SSE: {0}", clusteringError);
				Console.WriteLine();
			}
			Console.WriteLine("Total Clustering SSE: {0}", clusteringErrors.Sum());
			Console.WriteLine("Average Silhouette Score: {0}", CalculateAverageSilhouetteScore(features, clusterings));
			Console.WriteLine();
		}

		private double CalculateAverageSilhouetteScore(Matrix features, List<Clustering> clusterings)
		{
			int contributingNodes = 0;
			double averageSilhouetteScore = 0;
			for (int i = 0; i < clusterings.Count; i++)
			{
				var clustering = clusterings[i];
				for (int j = 0; j < clustering.Nodes.Count; j++)
				{
					// Get the average dissimilarity in cluster
					double sameClusterAverageDissimilarity = 0;
					for (int k = 0; k < clustering.Nodes.Count; k++)
					{
						// Don't compare against yourself
						if (j == k)
						{
							continue;
						}
						var firstNodeFeatures = features.Row(clustering.Nodes[j]);
						var secondNodeFeatures = features.Row(clustering.Nodes[k]);
						sameClusterAverageDissimilarity += CalculateNodeDistances(features, firstNodeFeatures, secondNodeFeatures);
					}

					if (clustering.Nodes.Count > 1)
					{
						sameClusterAverageDissimilarity /= (clustering.Nodes.Count - 1);
					}
					else
					{
						sameClusterAverageDissimilarity = 0;
					}

					double smallestOtherClusterDissimilarity = double.MaxValue;
					// Get the average dissimilarity in other clusters
					for (int k = 0; k < clusterings.Count; k++)
					{
						// Don't bother with the same clustering
						if (k == i)
						{
							continue;
						}
						var otherClustering = clusterings[k];
						double otherClusterAverageDissimilarity = 0;
						for (int l = 0; l < otherClustering.Nodes.Count; l++)
						{
							var firstNodeFeatures = features.Row(clustering.Nodes[j]);
							var secondNodeFeatures = features.Row(otherClustering.Nodes[l]);
							otherClusterAverageDissimilarity += CalculateNodeDistances(features, firstNodeFeatures, secondNodeFeatures);
						}
						otherClusterAverageDissimilarity /= (otherClustering.Nodes.Count);
						if (otherClusterAverageDissimilarity < smallestOtherClusterDissimilarity)
						{
							smallestOtherClusterDissimilarity = otherClusterAverageDissimilarity;
						}
					}

					// Add to silhouette score for this node if non zero
					if (sameClusterAverageDissimilarity != 0 && smallestOtherClusterDissimilarity != 0)
					{
						contributingNodes++;
						averageSilhouetteScore += (smallestOtherClusterDissimilarity - sameClusterAverageDissimilarity) / Math.Max(sameClusterAverageDissimilarity, smallestOtherClusterDissimilarity);
					}
				}
			}
			averageSilhouetteScore /= contributingNodes;
			return averageSilhouetteScore;
		}

		private List<Clustering> CreateKMeansClusters(Matrix features, List<Clustering> clusterings)
		{
			// Calculate the new centroids
			AddClusteringCentroids(features, clusterings);

			// Put the centroids into their own list
			List<Centroid> centroids = new List<Centroid>();
			foreach (var clustering in clusterings)
			{
				centroids.Add(clustering.Centroid);
			}

			var newClusterings = CalculateKMeansClustering(features, centroids);

			// See if anything changed
			bool changed = CompareClusterings(clusterings, newClusterings);

			if (!changed)
			{
				return newClusterings;
			}

			return CreateKMeansClusters(features, newClusterings);
		}

		private bool CompareClusterings(List<Clustering> firstClustering, List<Clustering> secondClustering)
		{
			for (int i = 0; i < firstClustering.Count; i++)
			{
				if (firstClustering[i].Nodes.Count != secondClustering[i].Nodes.Count)
				{
					return true;
				}

				for (int j = 0; j < firstClustering[i].Nodes.Count; j++)
				{
					firstClustering[i].Nodes.Sort();
					secondClustering[i].Nodes.Sort();

					if (firstClustering[i].Nodes[j] != secondClustering[i].Nodes[j])
					{
						return true;
					}
				}
			}
			return false;
		}

		private List<Clustering> CalculateKMeansClustering(Matrix features, List<Centroid> centroids)
		{
			var clusters = new List<Clustering>();
			foreach (var centroid in centroids)
			{
				var clustering = new Clustering
				{
					Centroid = centroid
				};
				clusters.Add(clustering);
			}
			for (int i = 0; i < features.Rows(); i++)
			{
				var feature = features.Row(i);
				int bestCentroidIndex = -1;
				double smallestDistance = double.MaxValue;
				for (int j = 0; j < clusters.Count; j++)
				{
					var centroid = clusters[j].Centroid;
					var distance = CalculateNodeDistances(features, feature, centroid.Features);
					if (distance < smallestDistance)
					{
						smallestDistance = distance;
						bestCentroidIndex = j;
					}
				}
				clusters[bestCentroidIndex].Nodes.Add(i);
			}
			return clusters;
		}

		private void AddClusteringCentroids(Matrix features, List<Clustering> clusters)
		{
			for (int i = 0; i < clusters.Count; i++)
			{
				double[] centroid = new double[NumberOfInputs];
				for (int j = 0; j < NumberOfInputs; j++)
				{
					int[] nominalCount = new int[features.StringToEnum[j].Count];
					int contributingNodes = 0;
					for (int k = 0; k < clusters[i].Nodes.Count; k++)
					{
						var node = clusters[i].Nodes[k];
						var nodeFeatures = features.Row(node);

						if (nodeFeatures[j] == double.MaxValue)
						{
							continue;
						}
						else if (features.StringToEnum[j].Count == 0)
						{
							centroid[j] += nodeFeatures[j];
							contributingNodes++;
						}
						else
						{
							nominalCount[(int)nodeFeatures[j]]++;
							contributingNodes++;
						}
					}
					if (features.StringToEnum[j].Count == 0)
					{
						centroid[j] = contributingNodes == 0 ? double.MaxValue : (centroid[j] / contributingNodes);
					}
					else
					{
						int maxValue = nominalCount.Max();
						int maxIndex = nominalCount.ToList().IndexOf(maxValue);
						centroid[j] = contributingNodes == 0 ? double.MaxValue : maxIndex;
					}
				}
				clusters[i].Centroid = new Centroid(centroid);
			}
		}

		private List<Clustering> CreateHACClusters(List<Clustering> clusters, double[][] distanceMatrix, int desiredNumberOfClusters)
		{
			// Base case (done whittling down to the desired number of clusters)
			if (clusters.Count == desiredNumberOfClusters)
			{
				return clusters;
			}

			double smallestDistance = double.MaxValue;
			int firstClusterIndex = -1;
			int secondClusterIndex = -1;
			// Find the two clusters that are closest to each other
			for (int i = 0; i < clusters.Count; i++)
			{
				for (int j = i + 1; j < clusters.Count; j++)
				{
					double clusterDistance = CLUSTER_DISTANCE_TYPE == ClusterDistanceType.Complete ? 0 : double.MaxValue;
					// Compare each node in the first cluster with every node in the other cluster
					for (int k = 0; k < clusters[i].Nodes.Count; k++)
					{
						var firstClusterNode = clusters[i].Nodes[k];
						for (int l = 0; l < clusters[j].Nodes.Count; l++)
						{
							var secondClusterNode = clusters[j].Nodes[l];
							var distance = distanceMatrix[firstClusterNode][secondClusterNode];
							if (CLUSTER_DISTANCE_TYPE == ClusterDistanceType.Complete && distance > clusterDistance)
							{
								clusterDistance = distance;
							}
							else if (CLUSTER_DISTANCE_TYPE == ClusterDistanceType.Single && distance < clusterDistance)
							{
								clusterDistance = distance;
							}
							else if (CLUSTER_DISTANCE_TYPE != ClusterDistanceType.Single && CLUSTER_DISTANCE_TYPE != ClusterDistanceType.Complete)
							{
								throw new Exception("Unknown cluster distance type");
							}
						}
					}

					if (clusterDistance < smallestDistance)
					{
						smallestDistance = clusterDistance;
						firstClusterIndex = i;
						secondClusterIndex = j;
					}
				}
			}

			var firstCluster = clusters[firstClusterIndex];
			var secondCluster = clusters[secondClusterIndex];

			//	Console.WriteLine("Merging {0} and {1}. Distance is {2}", firstClusterIndex, secondClusterIndex, smallestDistance);
			var combinedClusters = CombineClusters(firstCluster, secondCluster);
			clusters.Remove(secondCluster);
			clusters[firstClusterIndex] = combinedClusters;

			return CreateHACClusters(clusters, distanceMatrix, desiredNumberOfClusters);
		}

		private Clustering CombineClusters(Clustering firstClustering, Clustering secondClustering)
		{
			var combinedClusters = new Clustering();
			combinedClusters.Nodes.AddRange(firstClustering.Nodes);
			combinedClusters.Nodes.AddRange(secondClustering.Nodes);
			return combinedClusters;
		}

		private List<Centroid> InitializeKMeansCentroids(Matrix features, int desiredNumberOfClusters)
		{
			List<Centroid> centroids = new List<Centroid>();

			if (!USE_RANDOM_CENTROIDS)
			{
				for (int i = 0; i < desiredNumberOfClusters; i++)
				{
					var centroid = new Centroid
					{
						Features = features.Row(i)
					};
					centroids.Add(centroid);
				}
			}
			else
			{
				List<int> randomCentroidIndexes = new List<int>();
				while (randomCentroidIndexes.Count != desiredNumberOfClusters)
				{
					int randomIndex = RandomGenerator.Next(0, features.Rows());
					if (!randomCentroidIndexes.Contains(randomIndex))
					{
						randomCentroidIndexes.Add(randomIndex);
					}
				}
				foreach (var centroidIndex in randomCentroidIndexes)
				{

					var centroid = new Centroid
					{
						Features = features.Row(centroidIndex)
					};
					centroids.Add(centroid);
				}
			}
			return centroids;
		}

		private List<Clustering> InitializeHACClustering(Matrix features)
		{
			var clusters = new List<Clustering>();
			for (int i = 0; i < features.Rows(); i++)
			{
				var node = new Clustering();
				node.Nodes.Add(i);
				clusters.Add(node);
			}
			return clusters;
		}

		private double[][] CalculateDistanceMatrix(Matrix features)
		{
			int numberOfInstances = features.Rows();
			double[][] distanceMatrix = new double[numberOfInstances][];

			for (int i = 0; i < numberOfInstances; i++)
			{
				var firstFeature = features.Row(i);
				distanceMatrix[i] = new double[numberOfInstances];
				for (int j = 0; j < numberOfInstances; j++)
				{
					// A node's distance to itself is 0
					if (j == i)
					{
						distanceMatrix[i][j] = 0;
						continue;
					}
					var secondFeature = features.Row(j);

					distanceMatrix[i][j] = CalculateNodeDistances(features, firstFeature, secondFeature);
				}
			}

			return distanceMatrix;
		}

		private double CalculateNodeDistances(Matrix features, double[] firstFeature, double[] secondFeature)
		{
			double distance = 0;
			var nominalOrUnknownDistance = 0;
			for (int k = 0; k < NumberOfInputs; k++)
			{
				if ((firstFeature[k] == double.MaxValue || secondFeature[k] == double.MaxValue))
				{
					nominalOrUnknownDistance += 1;
				}
				else if (features.StringToEnum[k].Count != 0 && firstFeature[k] != secondFeature[k])
				{
					nominalOrUnknownDistance += 1;
				}
				else if (features.StringToEnum[k].Count == 0 && !USE_ALTERNATIVE_DISTANCE_MEASURE)
				{
					var delta = firstFeature[k] - secondFeature[k];
					distance += delta * delta;
				}
				else if (features.StringToEnum[k].Count == 0 && USE_ALTERNATIVE_DISTANCE_MEASURE)
				{
					var delta = firstFeature[k] - secondFeature[k];
					distance += Math.Abs(delta);
				}
			}
			return !USE_ALTERNATIVE_DISTANCE_MEASURE ? Math.Sqrt(distance + nominalOrUnknownDistance) : distance + nominalOrUnknownDistance;
		}

		internal class Clustering
		{
			public List<int> Nodes { get; set; } = new List<int>();
			public Centroid Centroid { get; set; }
		}

		internal class Centroid
		{
			public double[] Features { get; set; }
			public Centroid() { }
			public Centroid(double[] features)
			{
				Features = features;
			}
		}

		internal enum ClusterAlgorithm
		{
			HAC,
			KMeans
		}

		internal enum ClusterDistanceType
		{
			Single,
			Complete
		}
	}
}
