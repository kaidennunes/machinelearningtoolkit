﻿using MLToolkitCSharp.Models;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;

namespace MLToolkitCSharp.Algorithms
{
	public class DecisionTree : SupervisedLearner
	{
		private static readonly string CSV_PATH = Utils.ROOT_PATH + "CSV\\DecisionTree.csv";

		private readonly Random RandomGenerator;

		private static readonly double TRAINING_PERCENTAGE = 0.5;

		private DecisionTreeNode TreeRoot;

		private int NumberOfOutputs { get; set; }

		public DecisionTree() : this(new Random()) { }

		public DecisionTree(Random random)
		{
			RandomGenerator = random;
			TreeRoot = new DecisionTreeNode();
		}

		public override void Train(Matrix features, Matrix labels)
		{
			// Start over
			TreeRoot = new DecisionTreeNode();

			// Number of output classifications
			NumberOfOutputs = labels.StringToEnum.FirstOrDefault().Count;

			// Get the number of instances in the set
			int numberOfInstances = features.Rows();

			// Find the split in instances for the training and validation set
			int instanceSplit = (int)(TRAINING_PERCENTAGE * numberOfInstances);

			// Split the data into a training and a validation set
			Matrix trainingSetFeatures = new Matrix(features, 0, 0, instanceSplit, features.Cols());
			Matrix trainingSetLabels = new Matrix(labels, 0, 0, instanceSplit, 1);
			Matrix validationSetFeatures = new Matrix(features, instanceSplit, 0, numberOfInstances - instanceSplit, features.Cols());
			Matrix validationSetLabels = new Matrix(labels, instanceSplit, 0, numberOfInstances - instanceSplit, 1);

			// Recursively create the decision tree
			//CreateBranch(features, labels, TreeRoot);
			CreateBranch(trainingSetFeatures, trainingSetLabels, TreeRoot);

			// Print out the depth and number of nodes in the tree
			Console.WriteLine("Full Tree Number of nodes: " + TreeRoot.GetNodeCount());
			Console.WriteLine("Full Tree Tree depth: " + TreeRoot.GetDepth());

			// Recursively prune the decision tree
			PruneTree(validationSetFeatures, validationSetLabels, TreeRoot);

			// Print out the depth and number of nodes in the tree
			Console.WriteLine("Pruned Tree Number of nodes: " + TreeRoot.GetNodeCount());
			Console.WriteLine("Pruned Tree Tree depth: " + TreeRoot.GetDepth());
		}

		public override void Predict(double[] features, double[] labels)
		{
			labels[0] = GetOutput(TreeRoot, features);
		}

		private void PruneTree(Matrix features, Matrix labels, DecisionTreeNode decisionTreeNode)
		{
			// See if this node is a leaf node (i.e. has an output)
			var output = decisionTreeNode.Output;
			// If this node has an output, then don't worry about it, we won't try pruning
			if (output >= -0.5)
			{
				return;
			}

			// Otherwise, find the majority class for this node
			var majorityOutput = GetMajorityOutput(labels);

			// Get the current validation set accuracy
			var currentAccuracy = MeasureAccuracy(features, labels, null);

			// Save the current feature split
			var currentFeatureSplit = decisionTreeNode.FeatureIndex;

			// Prune off all the nodes from this node to see if the accuracy is adversely affected
			decisionTreeNode.FeatureIndex = -1;
			decisionTreeNode.Output = majorityOutput;
			var prunedAccuracy = MeasureAccuracy(features, labels, null);

			// If the accuracy wasn't adversely affected, keep the pruning changes and quit parsing this branch
			if (prunedAccuracy >= currentAccuracy)
			{
				decisionTreeNode.ChildrenNodes = new List<DecisionTreeNode>();
				return;
			}

			// Otherwise, the accuracy was adversely affected, put everything back the way it was and carry on the parsing
			decisionTreeNode.FeatureIndex = currentFeatureSplit;
			decisionTreeNode.Output = -1;

			// Go through each of the child nodes of this node to try pruning
			for (int i = 0; i < decisionTreeNode.ChildrenNodes.Count; i++)
			{
				// Try pruning this branch
				PruneTree(features, labels, decisionTreeNode.ChildrenNodes[i]);
			}
		}

		private double GetOutput(DecisionTreeNode treeNode, double[] features)
		{
			var output = treeNode.Output;
			// If that tree node doesn't have the answer, then look to its children nodes
			if (output < 0)
			{
				var featureIndex = treeNode.FeatureIndex;
				double[] prunedFeatures = new double[features.Length - 1];
				int i = 0;
				foreach (var feature in features)
				{
					if (i == featureIndex)
					{
						continue;
					}
					prunedFeatures[i++] = feature;
				}
				var childrenNodeIndex = (int)features[featureIndex];
				output = GetOutput(treeNode.ChildrenNodes[childrenNodeIndex], prunedFeatures);
			}

			return output;
		}

		private void CreateBranch(Matrix features, Matrix labels, DecisionTreeNode decisionTreeNode)
		{
			// Number of inputs
			int NumberOfInputs = features.Cols();

			// Store the information gain for each feature
			double[] gainRatio = new double[NumberOfInputs];

			// See if the decision tree has already classified enough
			var alreadyClassified = true;
			var firstLabelOutput = labels.Get(0, 0);
			for (int i = 0; i < labels.Rows(); i++)
			{
				if (labels.Get(i, 0) != firstLabelOutput)
				{
					alreadyClassified = false;
					break;
				}
			}

			if (alreadyClassified)
			{
				decisionTreeNode.Output = firstLabelOutput;
				return;
			}

			// If there was no way to get a pure class, then just give the best estimate
			if (NumberOfInputs == 0)
			{
				decisionTreeNode.Output = GetMajorityOutput(labels);
				return;
			}


			// Calculate info of current node
			int[] numberOfInstancesPerOutput = new int[NumberOfOutputs];
			for (int i = 0; i < features.Rows(); i++)
			{
				numberOfInstancesPerOutput[(int)labels.Get(i, 0)]++;
			}
			double currentNodeInfo = CalculateFeatureTypeInfo(features.Rows(), features.Rows(), numberOfInstancesPerOutput);


			// Calculate info of each feature
			for (int i = 0; i < NumberOfInputs; i++)
			{
				double featureInfo = 0;
				double splitEntropy = 0;
				// Calculate the info for each possible input type for that feature
				foreach (var featureType in features.StringToEnum[i])
				{
					int featureTypeInstanceCount = 0;
					int[] numberOfFeatureTypeInstancesPerOutput = new int[NumberOfOutputs];

					var featureTypeValue = featureType.Value;

					// Find out how many instances exist for that feature input type, as well as how many of those instances map to which output
					for (int j = 0; j < features.Rows(); j++)
					{
						var featureInstance = features.Get(j, i);

						if (featureInstance == featureTypeValue)
						{
							numberOfFeatureTypeInstancesPerOutput[(int)labels.Get(j, 0)]++;
							featureTypeInstanceCount++;
						}
					}

					featureInfo += CalculateFeatureTypeInfo(featureTypeInstanceCount, features.Rows(), numberOfFeatureTypeInstancesPerOutput);
					var featureTypeCountToInstancesRatio = featureTypeInstanceCount / (double)features.Rows();
					if (featureTypeCountToInstancesRatio == 0 || splitEntropy == double.MaxValue)
					{
						splitEntropy = double.MaxValue;
					}
					else
					{
						splitEntropy += (featureTypeInstanceCount / (double)features.Rows()) * Math.Log10(featureTypeCountToInstancesRatio) * -1;
					}
				}
				gainRatio[i] = (currentNodeInfo - featureInfo) / splitEntropy;
			}

			// Find which feature leaves us with the least amount of data left to process
			int bestInfoGainIndex = 0;
			double largestInfoGain = gainRatio[0];
			for (int i = 1; i < gainRatio.Length; i++)
			{
				if (gainRatio[i] > largestInfoGain)
				{
					bestInfoGainIndex = i;
					largestInfoGain = gainRatio[i];
				}
			}


			// Use the found best feature for this branch
			decisionTreeNode.FeatureIndex = bestInfoGainIndex;

			// For each possible feature input type, create that branch
			foreach (var featureInputType in features.StringToEnum[bestInfoGainIndex])
			{
				// Create new matrix that only have instances from this feature input type
				Matrix newFeatures = new Matrix(features, 0, 0, features.Rows(), features.Cols());
				Matrix newLabels = new Matrix(labels, 0, 0, labels.Rows(), labels.Cols());

				// Cut out all the instances that don't have the feature input type
				newFeatures.RemoveAllNonFeatureTypeRows(newLabels, bestInfoGainIndex, featureInputType.Value);

				// Remove this feature from the feature matrix
				newFeatures.RemoveColumn(bestInfoGainIndex);

				var branchDecisionNode = new DecisionTreeNode();
				// If there are no more instances, then just a best guess for this branch
				if (newFeatures.Rows() == 0)
				{
					branchDecisionNode.Output = GetMajorityOutput(labels);
				}
				else
				{
					CreateBranch(newFeatures, newLabels, branchDecisionNode);
				}
				decisionTreeNode.ChildrenNodes.Add(branchDecisionNode);
			}
		}

		private double GetMajorityOutput(Matrix labels)
		{
			var outputs = new int[NumberOfOutputs];
			for (int i = 0; i < labels.Rows(); i++)
			{
				var instanceOutput = labels.Get(i, 0);
				outputs[(int)instanceOutput]++;
			}

			int maxValue = outputs.Max();
			int maxIndex = outputs.ToList().IndexOf(maxValue);
			return maxIndex;
		}

		private double CalculateFeatureTypeInfo(int numberOfFeatureTypeInstances, int numberOfParentInstances, IEnumerable<int> numberOfFeatureTypeInstancesPerOutput)
		{
			double featureInstanceToParentRatio = numberOfFeatureTypeInstances / (double)numberOfParentInstances;
			double info = 0;

			foreach (var numberOfOutputInstances in numberOfFeatureTypeInstancesPerOutput)
			{
				if (numberOfOutputInstances == 0)
				{
					continue;
				}
				double featureInstanceToOutputRatio = numberOfOutputInstances / (double)numberOfFeatureTypeInstances;
				double outputInfo = featureInstanceToOutputRatio * -1 * Math.Log(featureInstanceToOutputRatio, 2);
				info += outputInfo;
			}
			return info * featureInstanceToParentRatio;
		}
	}
}
