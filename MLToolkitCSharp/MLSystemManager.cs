﻿// ----------------------------------------------------------------
// The contents of this file are distributed under the CC0 license.
// See http://creativecommons.org/publicdomain/zero/1.0/
// ----------------------------------------------------------------

using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using MLToolkitCSharp.Algorithms;

namespace MLToolkitCSharp
{
	public class MLSystemManager
	{
		/**
         *  When you make a new learning algorithm, you should add a line for it to this method.
         */
		public SupervisedLearner GetLearner(string model, Random rand)
		{
			if (model.Equals("baseline", StringComparison.OrdinalIgnoreCase))
			{
				return new BaselineLearner();
			}
			else if (model.Equals("perceptron", StringComparison.OrdinalIgnoreCase))
			{
				return new Perceptron(rand);
			}
			else if (model.Equals("backpropagation", StringComparison.OrdinalIgnoreCase))
			{
				return new BackPropagation(rand);
			}
			else if (model.Equals("decisiontree", StringComparison.OrdinalIgnoreCase))
			{
				return new DecisionTree(rand);
			}
			else if (model.Equals("nearestneighbor", StringComparison.OrdinalIgnoreCase))
			{
				return new NearestNeighbor(rand);
			}
			else if (model.Equals("cluster", StringComparison.OrdinalIgnoreCase))
			{
				return new Cluster(rand);
			}
			else
			{
				throw new Exception("Unrecognized model: " + model);
			}
		}

		public void Run(string[] args)
		{
			//args = new string[] { "-L", "baseline", "-A", "iris.arff", "-E", "cross", "10", "-N" };

			//Random rand = new Random(1234); // Use a seed for deterministic results (makes debugging easier)
			Random rand = new Random(); // No seed for non-deterministic results

			// Parse the command line arguments
			ArgParser parser = new ArgParser(args);
			string fileName = parser.ARFF; // File specified by the user
			string learnerName = parser.Learner; // Learning algorithm specified by the user
			string evalMethod = parser.Evaluation; // Evaluation method specified by the user
			string evalParameter = parser.EvalParameter; // Evaluation parameters specified by the user
			bool printConfusionMatrix = parser.Verbose;
			bool normalize = parser.Normalize;

			// Load the model
			SupervisedLearner learner = GetLearner(learnerName, rand);

			// Load the ARFF file
			Matrix data = new Matrix();
			data.LoadArff(fileName);
			if (normalize)
			{
				Console.WriteLine("Using normalized data\n");
				data.Normalize();
			}

			// Print some stats
			Console.WriteLine();
			Console.WriteLine("Dataset name: " + fileName);
			Console.WriteLine("Number of instances: " + data.Rows());
			Console.WriteLine("Number of attributes: " + data.Cols());
			Console.WriteLine("Learning algorithm: " + learnerName);
			Console.WriteLine("Evaluation method: " + evalMethod);
			Console.WriteLine();

			if (evalMethod.Equals("training", StringComparison.OrdinalIgnoreCase))
			{
				Console.WriteLine("Calculating accuracy on training set...");
				Matrix features = new Matrix(data, 0, 0, data.Rows(), data.Cols());
				Matrix labels = new Matrix(data, 0, data.Cols() - 1, data.Rows(), 1);
				Matrix confusion = new Matrix();
				Stopwatch stopwatch = new Stopwatch();
				stopwatch.Start();
				learner.Train(features, labels);
				stopwatch.Stop();
				Console.WriteLine("Time to train (in seconds): " + stopwatch.Elapsed.TotalSeconds);
				double accuracy = learner.MeasureAccuracy(features, labels, confusion);
				Console.WriteLine("Training set accuracy: " + accuracy);
				if (printConfusionMatrix)
				{
					Console.WriteLine("\nConfusion matrix: (Row=target value, Col=predicted value)");
					confusion.Print();
					Console.WriteLine("\n");
				}
			}
			else if (evalMethod.Equals("static", StringComparison.OrdinalIgnoreCase))
			{
				Matrix testData = new Matrix();
				testData.LoadArff(evalParameter);
				if (normalize)
				{
					testData.Normalize(); // BUG! This may normalize differently from the training data. It should use the same ranges for normalization!
				}

				Console.WriteLine("Calculating accuracy on separate test set...");
				Console.WriteLine("Test set name: " + evalParameter);
				Console.WriteLine("Number of test instances: " + testData.Rows());
				Matrix features = new Matrix(data, 0, 0, data.Rows(), data.Cols() - 1);
				Matrix labels = new Matrix(data, 0, data.Cols() - 1, data.Rows(), 1);
				Stopwatch stopwatch = new Stopwatch();
				stopwatch.Start();
				learner.Train(features, labels);
				stopwatch.Stop();
				Console.WriteLine("Time to train (in seconds): " + stopwatch.Elapsed.TotalSeconds);
				double trainAccuracy = learner.MeasureAccuracy(features, labels, null);
				Console.WriteLine("Training set accuracy: " + trainAccuracy);
				Matrix testFeatures = new Matrix(testData, 0, 0, testData.Rows(), testData.Cols() - 1);
				Matrix testLabels = new Matrix(testData, 0, testData.Cols() - 1, testData.Rows(), 1);
				Matrix confusion = new Matrix();
				double testAccuracy = learner.MeasureAccuracy(testFeatures, testLabels, confusion);
				Console.WriteLine("Test set accuracy: " + testAccuracy);
				if (printConfusionMatrix)
				{
					Console.WriteLine("\nConfusion matrix: (Row=target value, Col=predicted value)");
					confusion.Print();
					Console.WriteLine("\n");
				}
			}
			else if (evalMethod.Equals("random", StringComparison.OrdinalIgnoreCase))
			{
				Console.WriteLine("Calculating accuracy on a random hold-out set...");
				double trainPercent = 1 - double.Parse(evalParameter);
				if (trainPercent < 0 || trainPercent > 1)
					throw new Exception("Percentage for random evaluation must be between 0 and 1");
				Console.WriteLine("Percentage used for training: " + trainPercent);
				Console.WriteLine("Percentage used for testing: " + double.Parse(evalParameter));
				data.Shuffle(rand);
				int trainSize = (int)(trainPercent * data.Rows());
				Matrix trainFeatures = new Matrix(data, 0, 0, trainSize, data.Cols() - 1);
				Matrix trainLabels = new Matrix(data, 0, data.Cols() - 1, trainSize, 1);
				Matrix testFeatures = new Matrix(data, trainSize, 0, data.Rows() - trainSize, data.Cols() - 1);
				Matrix testLabels = new Matrix(data, trainSize, data.Cols() - 1, data.Rows() - trainSize, 1);
				Stopwatch stopwatch = new Stopwatch();
				stopwatch.Start();
				learner.Train(trainFeatures, trainLabels);
				stopwatch.Stop();
				Console.WriteLine("Time to train (in seconds): " + stopwatch.Elapsed.TotalSeconds);
		//		double trainAccuracy = learner.MeasureAccuracy(trainFeatures, trainLabels, null);
		//		Console.WriteLine("Training set accuracy: " + trainAccuracy);
				Matrix confusion = new Matrix();
				double testAccuracy = learner.MeasureAccuracy(testFeatures, testLabels, confusion);
				Console.WriteLine("Test set accuracy: " + testAccuracy);
				if (printConfusionMatrix || true)
				{
					Console.WriteLine("\nConfusion matrix: (Row=target value, Col=predicted value)");
					confusion.Print();
					Console.WriteLine("\n");
				}
			}
			else if (evalMethod.Equals("cross", StringComparison.OrdinalIgnoreCase))
			{
				Console.WriteLine("Calculating accuracy using cross-validation...");
				int folds = int.Parse(evalParameter);
				if (folds <= 0)
					throw new Exception("Number of folds must be greater than 0");
				Console.WriteLine("Number of folds: " + folds);
				int reps = 1;
				double trainSumAccuracy = 0.0;
				double testSumAccuracy = 0.0;
				double elapsedTime = 0.0;
				for (int j = 0; j < reps; j++)
				{
					data.Shuffle(rand);
					for (int i = 0; i < folds; i++)
					{
						int begin = i * data.Rows() / folds;
						int end = (i + 1) * data.Rows() / folds;
						Matrix trainFeatures = new Matrix(data, 0, 0, begin, data.Cols() - 1);
						Matrix trainLabels = new Matrix(data, 0, data.Cols() - 1, begin, 1);
						Matrix testFeatures = new Matrix(data, begin, 0, end - begin, data.Cols() - 1);
						Matrix testLabels = new Matrix(data, begin, data.Cols() - 1, end - begin, 1);
						trainFeatures.Add(data, end, 0, data.Rows() - end);
						trainLabels.Add(data, end, data.Cols() - 1, data.Rows() - end);
						Stopwatch stopwatch = new Stopwatch();
						stopwatch.Start();
						learner.Train(trainFeatures, trainLabels);
						stopwatch.Stop();
						elapsedTime += stopwatch.Elapsed.TotalSeconds;
						double trainAccuracy = learner.MeasureAccuracy(trainFeatures, trainLabels, null);
						double testAccuracy = learner.MeasureAccuracy(testFeatures, testLabels, null);
						trainSumAccuracy += trainAccuracy;
						testSumAccuracy += testAccuracy;
						Console.WriteLine("Rep=" + j + ", Fold=" + i + ", Train Accuracy=" + trainAccuracy + ", Test Accuracy=" + testAccuracy);
					}
				}
				elapsedTime /= (reps * folds);
				Console.WriteLine("Average time to train (in seconds): " + elapsedTime);
				Console.WriteLine("Mean train accuracy=" + (trainSumAccuracy / (reps * folds)));
				Console.WriteLine("Mean test accuracy=" + (testSumAccuracy / (reps * folds)));
			}
		}

		public static void Main(string[] args)
		{
			MLSystemManager systemManager = new MLSystemManager();
			systemManager.Run(args);
		}
	}
}
