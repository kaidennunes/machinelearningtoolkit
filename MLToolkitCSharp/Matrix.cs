﻿// ----------------------------------------------------------------
// The contents of this file are distributed under the CC0 license.
// See http://creativecommons.org/publicdomain/zero/1.0/
// ----------------------------------------------------------------

using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace MLToolkitCSharp
{
	public class Matrix
	{
		private static readonly string ARFF_PATH = Utils.ROOT_PATH + "ARFFS\\";
		// Data
		private List<double[]> Data { get; set; }

		// Meta-data
		private List<string> AttributeNames;
		public List<Dictionary<string, int>> StringToEnum { get; private set; }
		public List<Dictionary<int, string>> EnumToString { get; set; }

		private static readonly double MISSING = double.MaxValue; // representation of missing values in the dataset

		// Creates a 0x0 matrix. You should call loadARFF or setSize next.
		public Matrix() { }

		// Copies the specified portion of that matrix into this matrix
		public Matrix(Matrix that, int rowStart, int colStart, int rowCount, int colCount)
		{
			Data = new List<double[]>();
			for (int j = 0; j < rowCount; j++)
			{
				double[] rowSrc = that.Row(rowStart + j);
				double[] rowDest = new double[colCount];
				for (int i = 0; i < colCount; i++)
					rowDest[i] = rowSrc[colStart + i];
				Data.Add(rowDest);
			}
			AttributeNames = new List<string>();
			StringToEnum = new List<Dictionary<string, int>>();
			EnumToString = new List<Dictionary<int, string>>();
			for (int i = 0; i < colCount; i++)
			{
				AttributeNames.Add(that.AttrName(colStart + i));
				StringToEnum.Add(that.StringToEnum[colStart + i]);
				EnumToString.Add(that.EnumToString[colStart + i]);
			}
		}

		// Adds a copy of the specified portion of that matrix to this matrix
		public void Add(Matrix that, int rowStart, int colStart, int rowCount)
		{
			if (colStart + Cols() > that.Cols())
				throw new Exception("out of range");
			for (int i = 0; i < Cols(); i++)
			{
				if (that.ValueCount(colStart + i) != ValueCount(i))
					throw new Exception("incompatible relations");
			}
			for (int j = 0; j < rowCount; j++)
			{
				double[] rowSrc = that.Row(rowStart + j);
				double[] rowDest = new double[Cols()];
				for (int i = 0; i < Cols(); i++)
					rowDest[i] = rowSrc[colStart + i];
				Data.Add(rowDest);
			}
		}

		// Resizes this matrix (and sets all attributes to be continuous)
		public void SetSize(int rows, int cols)
		{
			Data = new List<double[]>();
			for (int j = 0; j < rows; j++)
			{
				double[] row = new double[cols];
				Data.Add(row);
			}
			AttributeNames = new List<string>();
			StringToEnum = new List<Dictionary<string, int>>();
			EnumToString = new List<Dictionary<int, string>>();
			for (int i = 0; i < cols; i++)
			{
				AttributeNames.Add("");
				StringToEnum.Add(new Dictionary<string, int>());
				EnumToString.Add(new Dictionary<int, string>());
			}
		}

		public void RemoveColumn(int column)
		{
			List<double[]> newData = new List<double[]>();
			foreach (var instance in Data)
			{
				double[] newInstance = new double[instance.Length - 1];
				int i = 0;
				foreach (var instanceFeature in instance)
				{
					if (i == column)
					{
						continue;
					}
					newInstance[i++] = instanceFeature;
				}
				newData.Add(newInstance);
			}

			Data = newData;

			EnumToString.RemoveAt(column);
			StringToEnum.RemoveAt(column);
			AttributeNames.RemoveAt(column);
		}

		public void RemoveInstance(Matrix labels, int instanceIndex)
		{
			List<double[]> newFeatureData = new List<double[]>();
			List<double[]> newLabelData = new List<double[]>();
			for (int i = 0; i < Rows(); i++)
			{
				var instance = Data[i];
				if (i != instanceIndex)
				{
					newFeatureData.Add(instance);
					newLabelData.Add(labels.Data[i]);
				}
			}

			Data = newFeatureData;
			labels.Data = newLabelData;
		}

		public void RemoveAllNonFeatureTypeRows(Matrix labels, int feature, int featureType)
		{
			List<double[]> newFeatureData = new List<double[]>();
			List<double[]> newLabelData = new List<double[]>();
			for (int i = 0; i < Rows(); i++)
			{
				var instance = Data[i];
				if (instance[feature] == featureType)
				{
					newFeatureData.Add(instance);
					newLabelData.Add(labels.Data[i]);
				}
			}

			Data = newFeatureData;
			labels.Data = newLabelData;
		}

		// Loads from an ARFF file
		public void LoadArff(string filename)
		{
			Data = new List<double[]>();
			AttributeNames = new List<string>();
			StringToEnum = new List<Dictionary<string, int>>();
			EnumToString = new List<Dictionary<int, string>>();
			bool READDATA = false;
			var filePath = Path.Combine(ARFF_PATH, filename);
			using (StreamReader s = new StreamReader(filePath))
			{
				while (!s.EndOfStream)
				{
					string line = s.ReadLine().Trim();
					if (line.Length > 0 && !line.StartsWith("%"))
					{
						if (!READDATA)
						{
							string[] tokens = line.Split((string[])null, StringSplitOptions.RemoveEmptyEntries);
							IEnumerator<string> t = tokens.AsEnumerable().GetEnumerator();

							t.MoveNext();
							string firstToken = t.Current;

							if (firstToken.Equals("@RELATION", StringComparison.OrdinalIgnoreCase))
							{
								t.MoveNext();
								string datasetName = t.Current;
							}

							if (firstToken.Equals("@ATTRIBUTE", StringComparison.OrdinalIgnoreCase))
							{
								Dictionary<string, int> ste = new Dictionary<string, int>();
								StringToEnum.Add(ste);
								Dictionary<int, string> ets = new Dictionary<int, string>();
								EnumToString.Add(ets);

								// Note: This port does not take into account quote escaped attributes as the Java
								// version does. It appears the C++ version of the toolkit doesn't do this either,
								// so hopefully this is justified.
								t.MoveNext();
								string attributeName = t.Current;
								if (line.Contains("'"))
								{
									string[] tokens2 = line.Split('\'');
									t = tokens2.AsEnumerable<string>().GetEnumerator();
									t.MoveNext();
									t.MoveNext();
									attributeName = "'" + t.Current + "'";
								}
								AttributeNames.Add(attributeName);

								int vals = 0;
								t.MoveNext();
								string type = t.Current.Trim();
								if (type.Equals("REAL", StringComparison.OrdinalIgnoreCase) || type.Equals("CONTINUOUS", StringComparison.OrdinalIgnoreCase) || type.Equals("INTEGER", StringComparison.OrdinalIgnoreCase))
								{
								}
								else
								{
									try
									{
										string[] values = line.Substring(line.IndexOf("{") + 1).Trim('}').Split(',');
										IEnumerator<string> v = values.AsEnumerable<string>().GetEnumerator();
										while (v.MoveNext())
										{
											string value = v.Current.Trim();
											if (value.Length > 0)
											{
												ste.Add(value, vals);
												ets.Add(vals, value);
												vals++;
											}
										}
									}
									catch (Exception e)
									{
										throw new Exception("Error parsing line: " + line + "\n" + e.Message);
									}
								}
							}

							if (firstToken.Equals("@DATA", StringComparison.OrdinalIgnoreCase))
							{
								READDATA = true;
							}
						}
						else
						{
							double[] newrow = new double[Cols()];
							int curPos = 0;

							try
							{
								string[] tokens = line.Split(',');
								IEnumerator<string> t = tokens.AsEnumerable().GetEnumerator();
								while (t.MoveNext())
								{
									String textValue = t.Current.Trim();
									//Console.WriteLine(textValue);

									if (textValue.Length > 0)
									{
										double doubleValue;
										int vals = EnumToString[curPos].Count;

										// Missing instances appear in the dataset as a double defined as MISSING
										if (textValue.Equals("?"))
										{
											doubleValue = MISSING;
										}
										// Continuous values appear in the instance vector as they are
										else if (vals == 0)
										//	if (vals == 0)
										{
											doubleValue = double.Parse(textValue);
										}
										// Discrete values appear as an index to the "name" 
										// of that value in the "attributeValue" structure
										else
										{
											doubleValue = StringToEnum[curPos][textValue];
											if (doubleValue == -1)
											{
												throw new Exception("Error parsing the value '" + textValue + "' on line: " + line);
											}
										}

										newrow[curPos] = doubleValue;
										curPos++;
									}
								}
							}
							catch (Exception e)
							{
								throw new Exception("Error parsing line: " + line + "\n" + e.Message);
							}
							Data.Add(newrow);
						}
					}
				}
			}
		}

		// Returns the number of rows in the matrix
		public int Rows() { return Data.Count; }

		// Returns the number of columns (or attributes) in the matrix
		public int Cols() { return AttributeNames.Count; }

		// Returns the specified row
		public double[] Row(int r) { return Data[r]; }

		// Returns the element at the specified row and column
		public double Get(int r, int c) { return Data[r][c]; }

		// Sets the value at the specified row and column
		public void Set(int r, int c, double v) { Row(r)[c] = v; }

		// Returns the name of the specified attribute
		public string AttrName(int col) { return AttributeNames[col]; }

		// Set the name of the specified attribute
		public void SetAttrName(int col, string name) { AttributeNames[col] = name; }

		// Returns the name of the specified value
		public string AttrValue(int attr, int val) { return EnumToString[attr][val]; }

		// Returns the number of values associated with the specified attribute (or column)
		// 0=continuous, 2=binary, 3=trinary, etc.
		public int ValueCount(int col) { return EnumToString[col].Count; }

		// Shuffles the row order
		public void Shuffle(Random rand)
		{
			for (int n = Rows(); n > 0; n--)
			{
				int i = rand.Next(n);
				double[] tmp = Row(n - 1);
				Data[n - 1] = Row(i);
				Data[i] = tmp;
			}
		}

		// Shuffles the row order along with the labels
		public void Shuffle(Random rand, Matrix labels)
		{
			for (int n = Rows(); n > 0; n--)
			{
				int i = rand.Next(n);

				double[] tmp = Row(n - 1);
				Data[n - 1] = Row(i);
				Data[i] = tmp;

				double[] tmpLabel = labels.Row(n - 1);
				labels.Data[n - 1] = labels.Row(i);
				labels.Data[i] = tmpLabel;
			}
		}

		// Returns the mean of the specified column
		public double ColumnMean(int col)
		{
			double sum = 0;
			int count = 0;
			for (int i = 0; i < Rows(); i++)
			{
				double v = Get(i, col);
				if (v != MISSING)
				{
					sum += v;
					count++;
				}
			}
			return sum / count;
		}

		// Returns the min value in the specified column
		public double ColumnMin(int col)
		{
			double m = MISSING;
			for (int i = 0; i < Rows(); i++)
			{
				double v = Get(i, col);
				if (v != MISSING)
				{
					if (m == MISSING || v < m)
					{
						m = v;
					}
				}
			}
			return m;
		}

		// Returns the max value in the specified column
		public double ColumnMax(int col)
		{
			double m = MISSING;
			for (int i = 0; i < Rows(); i++)
			{
				double v = Get(i, col);
				if (v != MISSING)
				{
					if (m == MISSING || v > m)
					{
						m = v;
					}
				}
			}
			return m;
		}

		// Returns the most common value in the specified column
		public double MostCommonValue(int col)
		{
			Dictionary<double, int> tm = new Dictionary<double, int>();
			for (int i = 0; i < Rows(); i++)
			{
				double v = Get(i, col);
				if (v != MISSING)
				{
					if (!tm.ContainsKey(v))
					{
						tm.Add(v, 1);
					}
					else
					{
						tm[v] = tm[v] + 1;
					}
				}
			}
			int maxCount = 0;
			double val = MISSING;
			Dictionary<double, int>.Enumerator it = tm.GetEnumerator();
			while (it.MoveNext())
			{
				KeyValuePair<double, int> e = it.Current;
				if (e.Value > maxCount)
				{
					maxCount = e.Value;
					val = e.Key;
				}
			}
			return val;
		}

		public void Normalize()
		{
			for (int i = 0; i < Cols(); i++)
			{
				if (ValueCount(i) == 0)
				{
					double min = ColumnMin(i);
					double max = ColumnMax(i);
					for (int j = 0; j < Rows(); j++)
					{
						double v = Get(j, i);
						if (v != MISSING)
						{
							Set(j, i, (v - min) / (max - min));
						}
					}
				}
			}
		}

		public void Print()
		{
			Console.WriteLine("@RELATION Untitled");
			for (int i = 0; i < AttributeNames.Count; i++)
			{
				Console.Write("@ATTRIBUTE " + AttributeNames[i]);
				int vals = ValueCount(i);
				if (vals == 0)
				{
					Console.WriteLine(" CONTINUOUS");
				}
				else
				{
					Console.Write(" {");
					for (int j = 0; j < vals; j++)
					{
						if (j > 0)
						{
							Console.Write(", ");
						}
						Console.Write(EnumToString[i][j]);
					}
					Console.WriteLine("}");
				}
			}
			Console.WriteLine("@DATA");
			for (int i = 0; i < Rows(); i++)
			{
				double[] r = Row(i);
				for (int j = 0; j < r.Length; j++)
				{
					if (j > 0)
					{
						Console.Write(", ");
					}
					if (ValueCount(j) == 0)
					{
						Console.Write(r[j]);
					}
					else
					{
						Console.Write(EnumToString[j][(int)r[j]]);
					}
				}
				Console.WriteLine("");
			}
		}
	}
}
